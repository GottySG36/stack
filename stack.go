// Utility to create graphs from a list of edges and extract the sub-graphs

package stack

import(
    "bitbucket.org/GottySG36/node"
)

type Stack []*node.Node

func (s Stack) Pop() (*node.Node, Stack) {
    return s[len(s)-1], s[:len(s)-1]
}

func (s Stack) Push(n ...*node.Node) (Stack) {
    return append(s, n...)
}

func (s Stack) IsEmpty() bool {
    return len(s) <= 0
}
